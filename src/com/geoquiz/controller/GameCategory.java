package com.geoquiz.controller;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum GameCategory {

    /**
     * Represents the category "Capitali".
     */
    CAPITALS("CAPITALI","Sai indicare la capitale di ciascun paese?","/images/capitali.jpg",CategoryType.MODE_WITH_DIFFICULTY_LEVEL,QuestionType.SINGLE_CHOICE_QUESTION),
    /**
     * Represents the category "Valute".
     */
    CURRENCIES("VALUTE","Sai indicare qual e' la valuta adottata da ciascun paese?","/images/valute.jpg",CategoryType.MODE_WITHOUT_DIFFICULTY_LEVEL,QuestionType.SINGLE_CHOICE_QUESTION),
    /**
     * Represents the category "Typical dishes".
     */
    DISHES("CUCINA","Sai indicare i paesi in base alla bandiera nazionale?","/images/cucina.jpg",CategoryType.MODE_WITHOUT_DIFFICULTY_LEVEL,QuestionType.SINGLE_CHOICE_QUESTION),
    /**
     * Represents the category "Bandiere".
     */
    FLAGS("BANDIERA","Sai indicare i paesi in base alla bandiera nazionale?","/images/bandiere.jpg",CategoryType.MODE_WITHOUT_DIFFICULTY_LEVEL,QuestionType.SINGLE_CHOICE_QUESTION),
    /**
     * Represents the category "Monumenti".
     */
    MONUMENTS("MONUMENTI","Sai indicare dove si trovano questi famosi monumenti?","/images/monumenti.jpg",CategoryType.MODE_WITH_DIFFICULTY_LEVEL,QuestionType.MULTIPLE_CHOICE_QUESTION);

    GameCategory(String buttonText, String categoryDescription, String imagePath, CategoryType categoryType, QuestionType questionType) {
        this.buttonText = buttonText;
        this.categoryDescription = categoryDescription;
        this.imagePath = imagePath;
        this.categoryType = categoryType;
        this.questionType = questionType;
    }

    private final String buttonText;
    private final String categoryDescription;
    private final String imagePath;
    private final CategoryType categoryType;
    private final QuestionType questionType;
    public CategoryType getCategoryType() {
        return categoryType;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public String getImagePath() {
        return imagePath;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }
}
