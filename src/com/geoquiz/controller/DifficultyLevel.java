package com.geoquiz.controller;

import com.geoquiz.model.quiz.ExtendedMode;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum DifficultyLevel {
    /**
     * Represents the difficulty level "Facile".
     */
    EASY("FACILE",ExtendedMode.EASY),
    /**
     * Represents the difficulty level "Medio".
     */
    MEDIUM("MEDIO",ExtendedMode.MEDIUM),
    /**
     * Represents the difficulty level "Difficile".
     */
    HARD("DIFFICILE",ExtendedMode.HARD);

    DifficultyLevel(String buttonText, ExtendedMode difficultyLevel) {
        this.buttonText = buttonText;
        this.difficultyLevel = difficultyLevel;
    }

    private final String buttonText;
    private final ExtendedMode difficultyLevel;

    public String getButtonText() {
        return buttonText;
    }

    public ExtendedMode getDifficultyLevel() {
        return difficultyLevel;
    }
}
