package com.geoquiz.controller.quiz;

import java.util.Optional;
import java.util.Set;

import javax.xml.bind.JAXBException;

import com.geoquiz.controller.DifficultyLevel;
import com.geoquiz.controller.GameCategory;
import com.geoquiz.controller.GameMode;
import com.geoquiz.model.quiz.Quiz;

/**
 * Implementation of QuizController interface.
 *
 */
public class QuizControllerImpl extends AbstractQuizController {


    /**
     * @param type
     *            the type of the quiz.
     * @param mode
     *            the mode of the quiz.
     * @param difficulty
     *            the difficulty of the quiz.
     * @throws JAXBException
     *             exception.
     */
    public QuizControllerImpl(final GameCategory type, final GameMode mode, final Optional<DifficultyLevel> difficulty)
            throws JAXBException {
        super(type, mode, difficulty);
    }

    @Override
    public boolean is5050Available() {
        return quiz.is5050Available();
    }

    @Override
    public Set<String> use5050() {
        return quiz.use5050();
    }


}
