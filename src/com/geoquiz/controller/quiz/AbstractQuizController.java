package com.geoquiz.controller.quiz;

import com.geoquiz.controller.DifficultyLevel;
import com.geoquiz.controller.GameCategory;
import com.geoquiz.controller.GameMode;
import com.geoquiz.model.quiz.BasicMode;
import com.geoquiz.model.quiz.Quiz;
import com.geoquiz.model.quiz.QuizFactory;
import com.geoquiz.utility.ResourceLoader;

import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of QuizController interface.
 *
 */
abstract public class AbstractQuizController implements QuizController {

    private static final double FREEZE_TIME = 3000;
    private static final String FLAGS_IMAGES_PATH = "/images/flags/";
    private List<String> selectedCorrectAnswers = new LinkedList<>();

    protected Quiz quiz;

    /**
     * @param type
     *            the type of the quiz.
     * @param mode
     *            the mode of the quiz.
     * @param difficulty
     *            the difficulty of the quiz.
     * @throws JAXBException
     *             exception.
     */
     public AbstractQuizController(final GameCategory type, final GameMode mode, final Optional<DifficultyLevel> difficulty)
            throws JAXBException {

        switch (type) {
            case CAPITALS:
                switch (mode){

                    case CLASSIC:
                        DifficultyLevel difficultyLevel = difficulty.get();
                        this.quiz = QuizFactory.createCapitalsQuiz(difficultyLevel.getDifficultyLevel());
                        break;
                    case CHALLENGE:
                        this.quiz = QuizFactory.createCapitalsQuiz(BasicMode.CHALLENGE);
                        break;
                    case TRAINING:
                        this.quiz = QuizFactory.createCapitalsQuiz(BasicMode.TRAINING);
                        break;
                }
                break;
            case CURRENCIES:
                switch (mode){
                    case CLASSIC:
                        this.quiz = QuizFactory.createCurrenciesQuiz(Optional.empty());
                        break;
                    case CHALLENGE:
                        this.quiz = QuizFactory.createCurrenciesQuiz(Optional.of(BasicMode.CHALLENGE));
                        break;
                    case TRAINING:
                        this.quiz = QuizFactory.createCurrenciesQuiz(Optional.of(BasicMode.TRAINING));
                        break;
                }
                break;
            case DISHES:
                switch (mode){
                    case CLASSIC:
                        this.quiz = QuizFactory.createTypicalDishesQuiz(Optional.empty());
                        break;
                    case CHALLENGE:
                        this.quiz = QuizFactory.createTypicalDishesQuiz(Optional.of(BasicMode.CHALLENGE));
                        break;
                    case TRAINING:
                        this.quiz = QuizFactory.createTypicalDishesQuiz(Optional.of(BasicMode.TRAINING));
                        break;
                }
                break;
            case FLAGS:
                switch (mode){
                    case CLASSIC:
                        this.quiz = QuizFactory.createFlagsQuiz(Optional.empty());
                        break;
                    case CHALLENGE:
                        this.quiz = QuizFactory.createFlagsQuiz(Optional.of(BasicMode.CHALLENGE));
                        break;
                    case TRAINING:
                        this.quiz = QuizFactory.createFlagsQuiz(Optional.of(BasicMode.TRAINING));
                        break;
                }
                break;
            case MONUMENTS:
                switch (mode){
                    case CLASSIC:
                        DifficultyLevel difficultyLevel = difficulty.get();
                        this.quiz = QuizFactory.createMonumentsQuiz(difficultyLevel.getDifficultyLevel());
                        break;
                    case CHALLENGE:
                        this.quiz = QuizFactory.createMonumentsQuiz(BasicMode.CHALLENGE);
                        break;
                    case TRAINING:
                        this.quiz = QuizFactory.createMonumentsQuiz(BasicMode.TRAINING);
                        break;
                }
                break;
            default:
            throw new IllegalArgumentException();

        }
    }

    @Override
    public String showStringQuestion() {
        return this.quiz.getCurrentQuestion().getQuestion();
    }

    @Override
    public InputStream showImageQuestion() {

        return ResourceLoader.loadResourceAsStream(FLAGS_IMAGES_PATH + this.quiz.getCurrentQuestion().getQuestion());

    }

    @Override
    public void hitAnswer(final Optional<String> answer) {
        this.quiz.hitAnswer(answer);
        if(answer.isPresent()){
            if(this.quiz.isAnswerCorrect()){
                this.selectedCorrectAnswers.add(answer.get());
            }
        }

    }

    @Override
    public void nextQuestion() {
        if (!this.gameOver()) {
            this.quiz.next();
            this.selectedCorrectAnswers = new LinkedList<>();
        }

    }

    @Override
    public boolean checkAnswer() {
        return this.quiz.isAnswerCorrect();
    }

    @Override
    public List<String> getCorrectAnswer() {
        return this.quiz.getCorrectAnswers();
    }

    @Override
    public int getRemainingLives() {
        return this.quiz.getRemainingLives();
    }

    @Override
    public boolean gameOver() {
        return this.quiz.gameOver();
    }

    @Override
    public boolean isFreezeAvailable() {
        return this.quiz.isFreezeAvailable();
    }

    @Override
    public boolean isSkipAvailable() {
        return this.quiz.isSkipAvailable();
    }


    @Override
    public double freeze() {
        this.quiz.freeze();
        return AbstractQuizController.FREEZE_TIME;
    }
    @Override
    public boolean isQuestionCompleted(){
         return this.selectedCorrectAnswers.containsAll(this.quiz.getCorrectAnswers());
    }

    @Override
    public void skip() {
        this.quiz.skip();
    }


    @Override
    public int getQuestionDuration() {
        return this.quiz.getQuestionDuration();
    }

    @Override
    public void restart() {
        this.quiz.restart();
    }

    @Override
    public Set<String> showAnswers() {
        return this.quiz.getCurrentQuestion().getAnswers();
    }

    @Override
    public int getScore() {
        return this.quiz.getCurrentScore();
    }

    @Override
    abstract public Set<String> use5050();
    @Override
    abstract public boolean is5050Available();

}
