package com.geoquiz.controller.quiz;

import com.geoquiz.controller.DifficultyLevel;
import com.geoquiz.controller.GameCategory;
import com.geoquiz.controller.GameMode;
import com.geoquiz.model.quiz.Quiz;

import javax.xml.bind.JAXBException;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of QuizController interface.
 *
 */
public class MultipleChoiceQuizControllerImpl extends AbstractQuizController {

    /**
     * @param type
     *            the type of the quiz.
     * @param mode
     *            the mode of the quiz.
     * @param difficulty
     *            the difficulty of the quiz.
     * @throws JAXBException
     *             exception.
     */
    public MultipleChoiceQuizControllerImpl(final GameCategory type, final GameMode mode, final Optional<DifficultyLevel> difficulty)
            throws JAXBException {
        super(type, mode, difficulty);
    }

    @Override
    public boolean is5050Available() {
        return false;
    }

    @Override
    public Set<String> use5050() {
        throw new IllegalStateException("No 50-50 in multiple choice quiz");
    }


}
