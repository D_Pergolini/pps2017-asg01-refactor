package com.geoquiz.controller;

public enum QuestionType {
    SINGLE_CHOICE_QUESTION,
    MULTIPLE_CHOICE_QUESTION;
}
