package com.geoquiz.controller;

public enum CategoryType {
    MODE_WITH_DIFFICULTY_LEVEL,
    MODE_WITHOUT_DIFFICULTY_LEVEL;
}
