package com.geoquiz.controller;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum GameMode {
    CLASSIC("CLASSICA"),
    /**
     * Represents the game mode "Sfida".
     */
    CHALLENGE("SFIDA"),
    /**
     * Represents the game mode "Allenamento".
     */
    TRAINING("ALLENAMENTO");

    GameMode(String buttonText) {
        this.buttonText = buttonText;
    }

    private final String buttonText;

    public String getButtonText() {
        return buttonText;
    }

}
