package com.geoquiz.view.menu;

import com.geoquiz.controller.GameCategory;
import com.geoquiz.view.resources.AccountManagementResources;
import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final double BUTTON_WIDTH = 350;

    private static final double USER_LABEL_FONT = 40;
    private static final Color BUTTON_COLOR = Color.BLUE;
    private static final Color COLOR_TEXT_USER_LABEL = Color.BLACK;

    private final Pane panel = new Pane();

    private final HBox hbox = new HBox(10);
    private final HBox hbox2 = new HBox(10);
    private final VBox vbox = new VBox();
//    private static Optional<GameCategory> selectedCategory = Optional.empty();
    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final Map<GameCategory,MyButton> allButton = new EnumMap<>(GameCategory.class);
        for(GameCategory gameCategory : GameCategory.values()){
                allButton.put(gameCategory,MyButtonFactory.createMyButton(
                                                gameCategory.getButtonText(),
                                                CategoryScene.BUTTON_COLOR,
                                                CategoryScene.BUTTON_WIDTH));
        }

        final MyButton back;

        final MyLabel userLabel = MyLabelFactory.createMyLabel(AccountManagementResources.TEXT_USER_LABEL+ LoginMenuScene.getUsername(), COLOR_TEXT_USER_LABEL,
                USER_LABEL_FONT);

        back = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);

        hbox.setTranslateX(POS_1_X);
        hbox.setTranslateY(POS_1_Y);

        hbox2.setTranslateX(POS_2_X);
        hbox2.setTranslateY(POS_2_Y);

        vbox.setTranslateX(POS_X_BACK);
        vbox.setTranslateY(POS_Y_BACK);

        hbox.getChildren().addAll((Node) allButton.get(GameCategory.FLAGS),
                (Node) allButton.get(GameCategory.CURRENCIES),
                (Node) allButton.get(GameCategory.DISHES));
        hbox2.getChildren().addAll((Node) allButton.get(GameCategory.MONUMENTS),
                (Node) allButton.get(GameCategory.CAPITALS));
        vbox.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        allButton.entrySet().forEach(myButton -> {
            ((Node) myButton.getValue()).setOnMouseClicked(event -> {
                this.playClickIfNotDisabled();
                final GameSettingsImpl.GameSettingsBuilder settingsBuilder = new GameSettingsImpl.GameSettingsBuilder();
                mainStage.setScene(new ModeScene(mainStage,settingsBuilder.setGameCategory(myButton.getKey())));
            });
        });

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);
    }

    private void playClickIfNotDisabled(){
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    }
}
