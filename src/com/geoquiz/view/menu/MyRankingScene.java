package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.geoquiz.utility.Pair;

import com.geoquiz.view.resources.SceneTitle;
import javafx.stage.Stage;

/**
 * The statistics scene where user can see own records.
 */
public final class MyRankingScene extends AbsoluteRankingScene {

    private static final String EMPTY_RECORD = "";
    private Map<Pair<String, String>, Integer> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws JAXBException
     *             for xml exception.
     */
    public MyRankingScene(final Stage mainStage) throws JAXBException {
        super(mainStage);
        super.getTitle().setText(SceneTitle.MY_RANKING_SCENE_TITLE.getTitle());
        try {
            map = super.getRankingManager().getPersonalRanking(LoginMenuScene.getUsername());
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        super.clearLabel();
        labelOfCategoryWithDifficultyLevel.entrySet().forEach(mapEntry-> {
            mapEntry.getValue().entrySet().forEach(gameModeMap ->{
                gameModeMap.getValue().setText(gameModeMap.getValue().getText()+this.getRecordByCategory(mapEntry.getKey().getButtonText(),gameModeMap.getKey().getButtonText()) );
            });
        });
        labelOfCategoryWithGameMode.entrySet().forEach(mapEntry-> {
            mapEntry.getValue().entrySet().forEach(gameModeMap ->{
                gameModeMap.getValue().setText(gameModeMap.getValue().getText()+this.getRecordByCategory(mapEntry.getKey().getButtonText(),gameModeMap.getKey().getButtonText()) );
            });
        });
    }

    private String getRecordByCategory(final String category, final String difficulty) {
        final Integer record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? EMPTY_RECORD : record.toString();
    }
}