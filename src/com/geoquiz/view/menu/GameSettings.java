package com.geoquiz.view.menu;

import com.geoquiz.controller.DifficultyLevel;
import com.geoquiz.controller.GameCategory;
import com.geoquiz.controller.GameMode;

import java.util.Optional;

public interface GameSettings {
    GameCategory getSelectedGameCategory();
    GameMode getSelectedGameMode();
    Optional<DifficultyLevel> getSelectedDifficultyLevel();
}
