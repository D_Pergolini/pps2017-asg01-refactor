package com.geoquiz.view.menu;

import com.geoquiz.controller.CategoryType;
import com.geoquiz.controller.GameCategory;
import com.geoquiz.controller.GameMode;
import com.geoquiz.view.resources.AccountManagementResources;
import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 * The scene where user can choose game modality.
 */
public class ModeScene extends Scene {
    private static final String MESSAGE_FOR_USER = "\nScegli prima la modalità di gioco!";
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 350;
    private static final double POS_3_Y = 200;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double BUTTON_WIDTH = 350;
    private static final double OPACITY = 0.5;
    private static final double USER_LABEL_FONT = 40;
    private static final Color COLOR_TEXT_USER_LABEL = Color.BLACK;
    private static final Color BUTTON_COLOR = Color.BLUE;
    private static final Font LABEL_FONT = Font.font("Italic", FontWeight.BOLD, 35);

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();
    private final VBox vbox3 = new VBox();
    private final Label label = new Label();
    private final GameCategory selectedCategory;
    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public ModeScene(final Stage mainStage,GameSettingsImpl.GameSettingsBuilder settingsBuilder) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());
        final MyButton back;
        final Map<GameMode,MyButton> allButton = new EnumMap<>(GameMode.class);
        for(GameMode gameMode : GameMode.values()){
            allButton.put(gameMode,MyButtonFactory.createMyButton(
                    gameMode.getButtonText(),
                    BUTTON_COLOR,
                    BUTTON_WIDTH));
        }

        final MyLabel userLabel = MyLabelFactory.createMyLabel(AccountManagementResources.TEXT_USER_LABEL+ LoginMenuScene.getUsername(), COLOR_TEXT_USER_LABEL,
                USER_LABEL_FONT);

        back = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);

        this.selectedCategory = settingsBuilder.getGameCategory();
        label.setText(selectedCategory.getCategoryDescription()+ MESSAGE_FOR_USER);

        label.setFont(LABEL_FONT);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);
        vbox.getChildren().addAll((Node)allButton.get(GameMode.CLASSIC),(Node)allButton.get(GameMode.CHALLENGE),(Node)allButton.get(GameMode.TRAINING));
        vbox2.getChildren().add((Node) back);
        vbox3.getChildren().add(label);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);
        vbox3.setTranslateX(POS_2_X);
        vbox3.setTranslateY(POS_3_Y);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            mainStage.setScene(new CategoryScene(mainStage));
        });
        allButton.entrySet().forEach(myButton -> {
            ((Node) myButton.getValue()).setOnMouseClicked(event -> {
                this.playClickIfNotDisabled();
                settingsBuilder.setGameMode(myButton.getKey());
                if(selectedCategory.getCategoryType()== CategoryType.MODE_WITH_DIFFICULTY_LEVEL){
                    mainStage.setScene(new LevelScene(mainStage,settingsBuilder));
                }else {
                    try {
                        mainStage.setScene(new QuizGamePlay(mainStage,settingsBuilder.build()));
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        });

        this.panel.getChildren().addAll(Background.createBackgroundImage(Optional.of(this.selectedCategory)), Background.createBackground(),
                Background.getLogo(), vbox, vbox2, vbox3, (Node) userLabel);

        this.setRoot(this.panel);

    }

    private void playClickIfNotDisabled(){
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    }
}
