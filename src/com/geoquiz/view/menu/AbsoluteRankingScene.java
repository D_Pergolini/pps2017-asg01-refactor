package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.Predicate;

import javax.xml.bind.JAXBException;

import com.geoquiz.controller.CategoryType;
import com.geoquiz.controller.DifficultyLevel;
import com.geoquiz.controller.GameCategory;
import com.geoquiz.controller.GameMode;
import com.geoquiz.controller.ranking.RankingManager;
import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;
import com.geoquiz.view.resources.SceneTitle;
import com.geoquiz.view.utility.Background;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The rankingManager scene where user can see other user's records.
 */
public class AbsoluteRankingScene extends Scene {
    private static final Color BUTTON_COLOR = Color.RED;
    private static final double POS_1_X = 20;
    private static final double POS_Y_BACK = 650;
    private static final double FONT = 35;
    private static final double BUTTON_WIDTH = 350;
    private static final double FONT_MODE = 25;
    private static final double POS_X_CATEGORY_BOX = 50;
    private static final double POS_Y_CATEGORY_BOX = 200;
    private static final double POS_X_CATEGORY_BOX_2 = 650;
    private static final double POS_Y_CATEGORY_BOX_2 = 75;
    private static final double POS_X_CAPITALS_BOX = 300;
    private static final double POS_Y_CAPITALS_BOX = 205;
    private static final double POS_X_MONUMENTS_BOX = 300;
    private static final double POS_Y_MONUMENTS_BOX = 450;
    private static final double POS_X_FLAGS_BOX = 850;
    private static final double POS_Y_FLAGS_BOX = 80;
    private static final double POS_X_CURRENCIES_BOX = 850;
    private static final double POS_Y_CURRENCIES_BOX = 325;
    private static final double POS_X_DISHES_BOX = 850;
    private static final double POS_Y_DISHES_BOX = 570;
    private static final double TITLE_FONT = 95;
    private static final String S = " -> ";


    private final MyLabel title;

    protected final Map<GameCategory,Map<DifficultyLevel,MyLabel>> labelOfCategoryWithDifficultyLevel =
            new EnumMap<>(GameCategory.class);
    protected final Map<GameCategory,Map<GameMode,MyLabel>> labelOfCategoryWithGameMode =
            new EnumMap<>(GameCategory.class);
    private final RankingManager rankingManager = RankingManager.getInstance();

    private Map<Pair<String, String>, Pair<String, Integer>> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws JAXBException
     *             for xml exception.
     */
    public AbsoluteRankingScene(final Stage mainStage) throws JAXBException {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MyButton indietro;

        title = MyLabelFactory.createMyLabel(SceneTitle.GLOBAL_RECORDS_TITLE.getTitle(), Color.BLACK, TITLE_FONT);
        try {
            map = this.rankingManager.getGlobalRanking();
        } catch (ClassNotFoundException | IOException e1) {
            e1.printStackTrace();
        }

        indietro = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);

        final Map<GameCategory,MyLabel> allLabel = new EnumMap<>(GameCategory.class);
        Arrays.stream(GameCategory.values()).forEach(gameCategory -> {
                    allLabel.put(gameCategory,MyLabelFactory.createMyLabel(
                    gameCategory.getButtonText(),
                    BUTTON_COLOR,
                    FONT));
        });

        createRecordLabels();

        VBox titleBox = new VBox();
        titleBox.getChildren().add((Node) title);


        VBox capitalsBox = new VBox();
        capitalsBox.getChildren().addAll(
                                            (Node) getLabelByCategoryAndDifficultyLevel(GameCategory.CAPITALS,DifficultyLevel.EASY),
                                            (Node) getLabelByCategoryAndDifficultyLevel(GameCategory.CAPITALS,DifficultyLevel.MEDIUM),
                                            (Node) getLabelByCategoryAndDifficultyLevel(GameCategory.CAPITALS,DifficultyLevel.HARD),
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.CAPITALS,GameMode.CHALLENGE)
                                            );
        VBox monumentsBox = new VBox();
        monumentsBox.getChildren().addAll(
                                            (Node) getLabelByCategoryAndDifficultyLevel(GameCategory.MONUMENTS,DifficultyLevel.EASY),
                                            (Node) getLabelByCategoryAndDifficultyLevel(GameCategory.MONUMENTS,DifficultyLevel.MEDIUM),
                                            (Node) getLabelByCategoryAndDifficultyLevel(GameCategory.MONUMENTS,DifficultyLevel.HARD),
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.MONUMENTS,GameMode.CHALLENGE)
                                            );

        VBox currenciesBox = new VBox();
        currenciesBox.getChildren().addAll(
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.CURRENCIES,GameMode.CLASSIC),
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.CURRENCIES,GameMode.CHALLENGE)
                                            );

        VBox flagsBox = new VBox();
        flagsBox.getChildren().addAll(
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.FLAGS,GameMode.CLASSIC),
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.FLAGS,GameMode.CHALLENGE)
                                        );

        VBox dishesBox = new VBox();
        dishesBox.getChildren().addAll(
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.DISHES,GameMode.CLASSIC),
                                            (Node) getLabelByCategoryAndGameMode(GameCategory.DISHES,GameMode.CHALLENGE)
                                        );

        VBox categoryBox = new VBox(200);
        categoryBox.getChildren().addAll(
                                            (Node) allLabel.get(GameCategory.CAPITALS),
                                            (Node) allLabel.get(GameCategory.MONUMENTS)
                                        );

        VBox categoryBox2 = new VBox(200);
        categoryBox2.getChildren().addAll(
                                            (Node) allLabel.get(GameCategory.FLAGS),
                                            (Node)allLabel.get(GameCategory.CURRENCIES),
                                            (Node) allLabel.get(GameCategory.DISHES)
                                    );


        categoryBox.setTranslateX(POS_X_CATEGORY_BOX);
        categoryBox.setTranslateY(POS_Y_CATEGORY_BOX);
        categoryBox2.setTranslateX(POS_X_CATEGORY_BOX_2);
        categoryBox2.setTranslateY(POS_Y_CATEGORY_BOX_2);
        capitalsBox.setTranslateX(POS_X_CAPITALS_BOX);
        capitalsBox.setTranslateY(POS_Y_CAPITALS_BOX);
        monumentsBox.setTranslateX(POS_X_MONUMENTS_BOX);
        monumentsBox.setTranslateY(POS_Y_MONUMENTS_BOX);
        dishesBox.setTranslateX(POS_X_DISHES_BOX);
        dishesBox.setTranslateY(POS_Y_DISHES_BOX);
        flagsBox.setTranslateX(POS_X_FLAGS_BOX);
        flagsBox.setTranslateY(POS_Y_FLAGS_BOX);
        currenciesBox.setTranslateX(POS_X_CURRENCIES_BOX);
        currenciesBox.setTranslateY(POS_Y_CURRENCIES_BOX);

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_Y_BACK);
        vbox.getChildren().add((Node) indietro);

        ((Node) indietro).setOnMouseClicked(event -> {
            MainWindow.playClickIfNotDisabled();
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, categoryBox,
                categoryBox2, capitalsBox, monumentsBox, flagsBox, currenciesBox, dishesBox, titleBox);

        this.setRoot(panel);
    }

    private void createRecordLabels(){
        Arrays.stream(GameCategory.values()).forEach(gameCategory -> {
            Predicate<GameMode> filterPredicate;
            if(gameCategory.getCategoryType()== CategoryType.MODE_WITH_DIFFICULTY_LEVEL){
                final EnumMap<DifficultyLevel,MyLabel> mapToadd = new EnumMap<>(DifficultyLevel.class);
                Arrays.stream(DifficultyLevel.values()).forEach(difficultyLevel -> {
                    final MyLabel labelToAdd = MyLabelFactory.createMyLabel(difficultyLevel.getButtonText() + S
                                    + this.getRecordbyCategory(gameCategory.getButtonText(), difficultyLevel.getButtonText()),
                            Color.BLACK, FONT_MODE);
                    mapToadd.put(difficultyLevel,labelToAdd);
                });
                filterPredicate = (gameMode) ->gameMode != GameMode.TRAINING && gameMode != GameMode.CLASSIC;
                labelOfCategoryWithDifficultyLevel.put(gameCategory,mapToadd);
            }else{
                filterPredicate = (gameMode) ->gameMode != GameMode.TRAINING;
            }

            final EnumMap<GameMode,MyLabel> mapToadd = new EnumMap<>(GameMode.class);
            Arrays.stream(GameMode.values()).filter(filterPredicate).forEach(gameMode -> {
                final MyLabel labelToAdd = MyLabelFactory.createMyLabel(gameMode.getButtonText() + S
                                + this.getRecordbyCategory(gameCategory.getButtonText(), gameMode.getButtonText()),
                        Color.BLACK, FONT_MODE);
                mapToadd.put(gameMode,labelToAdd);
            });
            labelOfCategoryWithGameMode.put(gameCategory,mapToadd);
        });
    }


    private String getRecordbyCategory(final String category, final String difficulty) {
        final Pair<String, Integer> record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.getX() + " " + "(" + record.getY() + ")";
    }

    /**
     * clear records labels.
     */
    protected void clearLabel() {

        labelOfCategoryWithDifficultyLevel.values().forEach(mapEntry-> {
            mapEntry.values().forEach(difficultyLevelMap->{
                difficultyLevelMap.setText(difficultyLevelMap.getText().substring(0,
                        difficultyLevelMap.getText().indexOf(" ", difficultyLevelMap.getText().indexOf(" ") + 1) + 1));
            });
        });
        labelOfCategoryWithGameMode.values().forEach(mapEntry-> {
            mapEntry.values().forEach(gameModeMap ->{
                gameModeMap.setText(gameModeMap.getText().substring(0,
                        gameModeMap.getText().indexOf(" ", gameModeMap.getText().indexOf(" ") + 1) + 1));
            });
        });
    }

    /**
     * gets the value of title label.
     * 
     * @return title label.
     */
    protected MyLabel getTitle() {
        return title;
    }

    /**
     * gets the value of facilecap label.
     * 
     * @return facilecap label.
     */
    private MyLabel getLabelByCategoryAndDifficultyLevel(GameCategory gameCategory,DifficultyLevel difficultyLevel){
        return labelOfCategoryWithDifficultyLevel.get(gameCategory).get(difficultyLevel);
    }
    private MyLabel getLabelByCategoryAndGameMode(GameCategory gameCategory,GameMode gameMode){
        return labelOfCategoryWithGameMode.get(gameCategory).get(gameMode);
    }


    /**
     * gets the controller.
     * 
     * @return controller.
     */
    protected RankingManager getRankingManager() {
        return this.rankingManager;
    }
}
