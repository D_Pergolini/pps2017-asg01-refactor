package com.geoquiz.view.menu;

import com.geoquiz.controller.DifficultyLevel;
import com.geoquiz.controller.GameCategory;
import com.geoquiz.controller.GameMode;

import java.util.Objects;
import java.util.Optional;

public class GameSettingsImpl implements GameSettings{
    private final GameCategory gameCategory;
    private final GameMode gameMode;
    private final Optional<DifficultyLevel> difficultyLevel;

    private GameSettingsImpl(GameCategory gameCategory, GameMode gameMode, Optional<DifficultyLevel> difficultyLevel) {
        this.gameCategory = gameCategory;
        this.gameMode = gameMode;
        this.difficultyLevel = difficultyLevel;
    }

    @Override
    public GameCategory getSelectedGameCategory() {
        return this.gameCategory;
    }

    @Override
    public GameMode getSelectedGameMode() {
        return this.gameMode;
    }

    @Override
    public Optional<DifficultyLevel> getSelectedDifficultyLevel() {
        return this.difficultyLevel;
    }
    public static class GameSettingsBuilder{
        private  GameCategory gameCategory;
        private  GameMode gameMode;
        private  Optional<DifficultyLevel> difficultyLevel = Optional.empty();

        public GameSettingsBuilder setGameCategory(GameCategory gameCategory) {
            this.gameCategory = gameCategory;
            return this;
        }

        public GameSettingsBuilder setGameMode(GameMode gameMode) {
            this.gameMode = gameMode;
            return this;
        }

        public GameSettingsBuilder setDifficultyLevel(Optional<DifficultyLevel> difficultyLevel) {
            this.difficultyLevel = difficultyLevel;
            return this;
        }

        public GameCategory getGameCategory() {
            return gameCategory;
        }

        public GameMode getGameMode() {
            return gameMode;
        }

        public Optional<DifficultyLevel> getDifficultyLevel() {
            return difficultyLevel;
        }

        public GameSettings build(){
            return new GameSettingsImpl(Objects.requireNonNull(gameCategory),Objects.requireNonNull(gameMode),difficultyLevel);
        }
    }

}
