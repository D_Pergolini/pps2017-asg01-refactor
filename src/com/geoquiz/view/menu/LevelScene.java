package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.bind.JAXBException;

import com.geoquiz.controller.DifficultyLevel;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;
import com.geoquiz.view.resources.AccountManagementResources;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.button.MyButton;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
public class LevelScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double BUTTON_WIDTH = 350;
    private static final double USER_LABEL_FONT = 40;
    private static final Color COLOR_TEXT_USER_LABEL = Color.BLACK;
    private static final Color BUTTON_COLOR = Color.BLUE;
    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public LevelScene(final Stage mainStage, GameSettingsImpl.GameSettingsBuilder settingsBuilder) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MyLabel userLabel = MyLabelFactory.createMyLabel(AccountManagementResources.TEXT_USER_LABEL+ LoginMenuScene.getUsername(), COLOR_TEXT_USER_LABEL,
                USER_LABEL_FONT);


        final MyButton back;

        back = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), Color.BLUE, BUTTON_WIDTH);
        final Map<DifficultyLevel,MyButton> allButton = new EnumMap<>(DifficultyLevel.class);
        for(DifficultyLevel difficultyLevel : DifficultyLevel.values()){
            allButton.put(difficultyLevel,MyButtonFactory.createMyButton(
                    difficultyLevel.getButtonText(),
                    BUTTON_COLOR,
                    BUTTON_WIDTH));
        }

        vbox.getChildren().addAll((Node) allButton.get(DifficultyLevel.EASY),
                                (Node) allButton.get(DifficultyLevel.MEDIUM),
                                (Node) allButton.get(DifficultyLevel.HARD));

        vbox2.getChildren().add((Node) back);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            mainStage.setScene(new CategoryScene(mainStage));
        });

        allButton.entrySet().forEach(myButton -> {
            ((Node) myButton.getValue()).setOnMouseClicked(event -> {
                this.playClickIfNotDisabled();
                settingsBuilder.setDifficultyLevel(Optional.of(myButton.getKey()));
                try {
                    mainStage.setScene(new QuizGamePlay(mainStage,settingsBuilder.build()));
                } catch (JAXBException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });

        this.panel.getChildren().addAll(Background.createBackgroundImage(Optional.of(settingsBuilder.getGameCategory())), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);

    }

    private void playClickIfNotDisabled(){
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    }
}
