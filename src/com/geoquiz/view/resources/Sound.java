package com.geoquiz.view.resources;

public enum Sound {
    GEO_QUIZ_MUSIC("/media/geoMusic.mp3"),
    CLICK_SOUND("/media/click.wav");

    Sound(String soundPath) {
        this.soundPath = soundPath;
    }

    private final String soundPath;

    public String getSoundPath() {
        return soundPath;
    }
}
