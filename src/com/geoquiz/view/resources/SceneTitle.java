package com.geoquiz.view.resources;

public enum SceneTitle {
    GLOBAL_RECORDS_TITLE("Global records"),
    MY_RANKING_SCENE_TITLE("My records");
    private final String title;

    SceneTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
