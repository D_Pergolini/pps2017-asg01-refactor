package com.geoquiz.view.resources;

public final class AccountManagementResources {
    public static final String PROMPT_TEXT_USERNAME = "Username";
    public static final String PROMPT_TEXT_PASSWORD = "Password";
    public static final String MISSING_USERNAME_OR_PASSWORD_MSG = "Username o password mancanti!";
    public static final String SPACES_NOT_ALLOWED_MSG = "Errore! Gli spazi non sono consentiti!";
    public static final String ALREADY_EXISTENT_USERNAME = "Errore! Username gia' esistente!";
    public static final String NAMEFILE = "account.txt";
    public static final String LOGIN_ERROR_MSG = "Errore! Username o password errati!";
    public static final String TEXT_USER_LABEL = "USER: ";
    private AccountManagementResources(){
        throw new IllegalStateException();
    }
}
